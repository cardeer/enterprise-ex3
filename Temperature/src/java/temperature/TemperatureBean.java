/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package temperature;

import javax.ejb.Stateless;

/**
 *
 * @author siwakorn
 */
@Stateless
public class TemperatureBean implements TemperatureBeanRemote {

    @Override
    public double fToC(double f) {
        return (5.0 / 9.0) * (f - 32.0);
    }

    // Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Business Method")
}
