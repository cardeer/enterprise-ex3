/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package temperature;

import javax.ejb.Remote;

/**
 *
 * @author siwakorn
 */
@Remote
public interface TemperatureBeanRemote {

    double fToC(double f);
    
}
